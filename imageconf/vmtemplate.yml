---
name: voipstack-freeswitch-v2-vm
provider:
  name: kvm
bootstrapper:
  workspace: /target/os-images
  mirror: https://debian.packages.managed-infra.com/debian/
  include_packages:
   - initramfs-tools
   - linux-headers-amd64
   - whois
   - psmisc
   - apt-transport-https
   - ca-certificates
   - ssl-cert
system:
  release: bullseye
  architecture: amd64
  hostname: voipstack-freeswitch-v2-vm
  bootloader: grub
  charmap: UTF-8
  locale: en_US
  timezone: Europe/Berlin
volume:
  backing: qcow2
  partitions:
    type: gpt
    boot:
      filesystem: ext4
      size: 1GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
    swap:
      size: 2GiB
      mountopts:
        - sw
        - discard
        - noatime
        - errors=remount-ro
    root:
      filesystem: ext4
      size: 7GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
    var/log:
      filesystem: ext4
      size: 5GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
packages:
  install:
    # debian standard packages
    - bash-completion
    - bzip2
    - zstd
    - dbus
    - file
    - locales
    - lsof
    - mime-support
    - openssh-client
    - openssh-server
    - python
    # custom standard packages
    - chrony
    - unp
    - locate
    - lsscsi
    - sg3-utils
    - unattended-upgrades
    - debsecan
    - vim
    - nano
    - vim-nox
    - unzip
    - rsync
    - davfs2
    - bzip2
    - zstd
    - binutils
    - sudo
    - anacron
    - screen
    - tree
    - zram-tools
    - haveged
    - needrestart
    - rng-tools
    - resolvconf
    - wget
    - curl
    # network tools
    - ethtool
    - wpasupplicant
    - mstflint
    - iproute2
    - dante-client
    - proxychains
    - proxychains4
    - stunnel4
    - net-tools
    - mtr
    - traceroute
    - ebtables
    - tcpdump
    - snmpd
    - snmp
    - snmp-mibs-downloader
    - lldpd
    - ipxe
    - ipxe-qemu
    - memtest86+
    # switching packages
    - bridge-utils
    - ifenslave
    # backup
    - borgbackup
    - proxmox-backup-client
    - etckeeper
    # email smarthost
    - dma
    - mailutils
    - mutt
    # cloud specific packages
    - cloud-init
    - salt-minion
    - puppet-agent
    - chef
    - qemu-guest-agent
    - open-vm-tools
    # user access management (directory)
    #- freeipa-client # missing in bullseye
    - sssd
    - sssd-dbus
    - libsss-sudo
    - libsss-idmap0
    - libsss-nss-idmap0
    - krb5-config
    - krb5-k5tls
    - krb5-user
    # - libpam-krb5
    - libpam-ccreds
    # monitoring
    - lm-sensors
    - nvme-cli
    - apcupsd
    - smartmontools
    - monit
    - icinga2
    - lynis
    - trivy
    - fio
    - zabbix-agent
    - omi
    - scx
    - watchdog
    - monitoring-plugins
    - monitoring-plugins-basic
    - monitoring-plugins-standard
    - monitoring-plugins-btrfs
    - nagios-plugins-contrib
    - nagios-snmp-plugins
    - nagios-plugins-rabbitmq
    - nagios-check-xmppng
    - libmonitoring-plugin-perl
    - libipc-run-perl
    - liblist-moreutils-perl
    - filebeat
    - metricbeat
    - auditbeat
    - heartbeat-elastic
    - packetbeat
    # security
    - cracklib-runtime
    - wazuh-agent
    - auditd
    - audispd-plugins
    - ipset
    - conntrack
    - shorewall
    - shorewall6
    - rkhunter
    - fail2ban
    - crowdsec
    - crowdsec-firewall-bouncer-iptables
    # system management
    - aptitude
    - rpm
    - apt-file
    - apt-show-versions
    - apt-listchanges
    - debsums
    - atop
    - iotop
    - nload
    - neofetch
    # database
    - mariadb-client
    - libmariadb-dev
    - msodbcsql17
    - mssql-tools
    - unixodbc
    - unixodbc-dev
    - freetds-dev
    - freetds-bin
    - tdsodbc
    # build toolchain
    - build-essential
    - make
    # cluster tools
    - pacemaker
    - heartbeat
    - corosync
    - keepalived
    - exabgp
    # scripting tools
    - python3
    - python3-dev
    - python3-venv
    - python3-pip
    - python3-setuptools
    - python3-wheel
    # callswitch packages
    - freeswitch-meta-all
    - smstools
    # fax tools
    - iaxmodem
    - hylafax-server
    - hylafax-client
    - uuid-runtime
    - xvfb
    - xauth
    - wkhtmltopdf
    # sip debug packages
    - sngrep
    # administration
    #- webmin
  install_standard: false
  mirror: https://debian.packages.managed-infra.com/debian/
  security: https://debian-security.packages.managed-infra.com/
  sources:
    debian-backports:
      - deb https://debian.packages.managed-infra.com/debian bullseye-backports main contrib non-free
    debian-fasttrack:
      - deb https://debian-fasttrack.packages.managed-infra.com/debian/ bullseye-fasttrack main contrib
      # - deb https://debian-fasttrack.packages.managed-infra.com/debian/ bullseye-backports main contrib
    pbs-client:
      - deb https://proxmox.packages.managed-infra.com/debian/pbs-client bullseye main
    icinga:
      - deb https://icinga.packages.managed-infra.com/debian icinga-bullseye main
    zabbix:
      - deb https://zabbix.packages.managed-infra.com/zabbix/6.5/debian bullseye main
      - deb-src https://zabbix.packages.managed-infra.com/zabbix/6.5/debian bullseye main
    microsoft:
      - deb https://microsoft.packages.managed-infra.com/debian/11/prod bullseye main
    aquasecurity:
      - deb https://aquasecurity.packages.managed-infra.com/trivy-repo/deb bullseye main
    crowdsec:
      - deb https://packagecloud.packages.managed-infra.com/crowdsec/crowdsec/any/ any main
    newrelic-infra:
      - deb https://newrelic.packages.managed-infra.com/infrastructure_agent/linux/apt bullseye main
    webmin:
      - deb https://webmin.packages.managed-infra.com/download/repository sarge contrib
    freeswitch:
      - deb https://freeswitch.packages.managed-infra.com/repo/deb/debian-release/ bullseye main
      - deb-src https://freeswitch.packages.managed-infra.com/repo/deb/debian-release/ bullseye main
    debmultimedia:
      - deb https://deb-multimedia.packages.managed-infra.com bullseye main non-free
      - deb https://deb-multimedia.packages.managed-infra.com bullseye-backports main
    mariadb:
      - deb https://mariadb.packages.managed-infra.com/repo/11.4/debian bullseye main
      - deb-src https://mariadb.packages.managed-infra.com/repo/11.4/debian bullseye main
      - deb https://mariadb-downloads.packages.managed-infra.com/Tools/debian bullseye main
    postgresql:
      - deb https://postgresql-apt.packages.managed-infra.com/pub/repos/apt/ bullseye-pgdg main
    elasticsearch:
      - deb https://elastic.packages.managed-infra.com/packages/8.x/apt stable main
    puppet:
      - deb https://puppetlabs-apt.packages.managed-infra.com bullseye puppet
    chef:
      - deb https://chef.packages.managed-infra.com/repos/apt/stable bullseye main
    saltstack:
      - deb https://saltstack.packages.managed-infra.com/artifactory/saltproject-deb/ stable main
    wazuh:
      - deb https://wazuh.packages.managed-infra.com/4.x/apt/ stable main
    cisofy:
      - deb https://cisofy.packages.managed-infra.com/community/lynis/deb/ stable main
    falcosecurity:
      - deb https://falco.packages.managed-infra.com/packages/deb stable main
  components:
    - main
    - contrib
    - non-free
  trusted-keys:
    - trusted-repo-keys/trusted-repo-keys.gpg
  apt.conf.d:
    00InstallRecommends: >-
      APT::Install-Recommends "false";
      APT::Install-Suggests   "false";
    00RetryDownload: 'APT::Acquire::Retries "3";'
plugins:
  tmpfs_workspace: {}
  admin_user:
    username: admin
    password: ch4ng3m3
    pubkey: ../authorized_keys
  apt_proxy:
    address: 127.0.0.1
    port: 8000
  debconf: >-
    d-i pkgsel/install-language-support boolean false
    popularity-contest popularity-contest/participate boolean false
    resolvconf resolvconf/linkify-resolvconf boolean true
  file_copy:
    files:
      - { src: ../copy-to-filesystem.tar.gz,
          dst: /opt/copy-to-filesystem.tar.gz,
          permissions: -rwxrwxrwx,
          owner: root,
          group: root }
  commands:
    commands:
      - ['chroot {root} tar --strip-components 1 -xvzf /opt/copy-to-filesystem.tar.gz -C /']
      - ['chroot {root} chmod 770 /usr/sbin/cmdb-deploy']
      - ['chroot {root} chmod 770 /usr/bin/cleanup_image']
      - ['chroot {root} /usr/bin/cleanup_image']
  minimize_size:
    zerofree: true
    apt:
      autoclean: true
